import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

class Component extends React.Component {
    render() {
        const { classes } = this.props
        return (
            <div className={classes.root}>
                <AppBar position="static" style={{ backgroundColor: 'transparent' }} align='center'>

                    <Typography className={classes.header}>
                        Sweet Pumpkins
                        </Typography>

                </AppBar>
            </div>
        )
    }
}

export default Component