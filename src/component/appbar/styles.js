const Styles = (theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    header: {
        color: '#ff6d00',
        fontWeight: 'bold',
        fontSize: 30
    }
}));

export default Styles
