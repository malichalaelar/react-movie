import React from 'react'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Typography } from '@material-ui/core';
class Component extends React.Component {
    render() {
        const { classes } = this.props
        return (
            <Card className={classes.card}>
                <CardMedia
                    className={classes.media}
                    image="/static/images/cards/contemplative-reptile.jpg"
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography>Judul</Typography>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Typography>Year</Typography>
                        <Typography>Rating</Typography>
                    </div>
                    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                        <Typography>tahun</Typography>
                        <Typography>tahun</Typography>
                    </div>
                </CardContent>
            </Card>
        )
    }
}

export default Component