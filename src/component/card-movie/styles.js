const Styles = (theme => ({
    card: {
        width: 330,
        marginTop: 20,
    },
    media: {
        height: 140,
    },
}));

export default Styles