import React from 'react'
import Appbar from '../../component/appbar'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Card from '../../component/card-movie'

class Component extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Appbar />
                <Box>
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <h3>navigation</h3>
                        </Grid>
                        <Grid item xs={8}>
                            <Card />
                        </Grid>
                    </Grid>
                </Box>
            </React.Fragment>
        )
    }
}

export default Component